***Settings***
Documentation               Aqui teremos as KWs helpers

***Variables***
${TEXT_START}               xpath=//android.view.View[@content-desc="ProJuris Mobile"]
${ELEMENT_EMAIL}            xpath=//android.widget.ImageView[@text="E-mail"]
${ELEMENT_SENHA}            xpath=//android.widget.ImageView[@text="Senha"]
${BOTAO_ENTRAR}             xpath=//android.widget.Button[@content-desc="Entrar"]
${MSG_SEM_DADOS}            xpath=//android.view.View[@content-desc="E-mail é obrigatório"]
${MSG_SEM_SENHA}            xpath=//android.view.View[@content-desc="Senha é obrigatória"]
${MSG_LOGIN_SEM_SUCESSO}    xpath=//android.view.View[@content-desc="E-mail ou senha inválidos"]
${ESQUECEU_SENHA}           xpath=//android.widget.Button[@content-desc="Esqueci minha senha"]
${RECUPERAR_SENHA}          xpath=//android.widget.Button[@content-desc="Recuperar Senha"]
${MSG_RECUPERA_SENHA}       xpath=//android.view.View[@content-desc="Foi enviado um email com a sua nova senha!"]

***Keywords***
Open Nav
    Wait Until Element Is Visible       ${TEXT_START}
    Swipe By Percent                    78.79                                                     81.25       47.68          81.25
    Swipe By Percent                    78.79                                                     81.25       47.68          81.25
    Swipe By Percent                    78.79                                                     81.25       47.68          81.25
    Swipe By Percent                    78.79                                                     81.25       47.68          81.25
    Swipe By Percent                    78.79                                                     81.25       47.68          81.25
    Swipe By Percent                    78.79                                                     81.25       47.68          81.25
    Click Element                       xpath=//android.widget.Button[@content-desc="Iniciar"]

Go To Login Form
    Open Nav

Login com sucesso
    [Arguments]                         ${email}                                                  ${senha}    ${login_ok}

    Wait Until Element Is Visible       ${ELEMENT_EMAIL}
    Click Element                       ${ELEMENT_EMAIL}
    Execute Adb Shell                   input text                                                ${email}
    Click Element                       ${ELEMENT_SENHA}
    Execute Adb Shell                   input text                                                ${senha}
    Hide Keyboard
    Click Element                       ${BOTAO_ENTRAR}
    Wait Until Page Contains            ${login_ok}

Login incorreto
    [Arguments]                         ${email}                                                  ${senha}

    Wait Until Element Is Visible       ${ELEMENT_EMAIL}
    Click Element                       ${ELEMENT_EMAIL}
    Execute Adb Shell                   input text                                                ${email}
    Click Element                       ${ELEMENT_SENHA}
    Execute Adb Shell                   input text                                                ${senha}
    Hide Keyboard
    Click Element                       ${BOTAO_ENTRAR}
    Wait Until Element Is Visible       ${MSG_LOGIN_SEM_SUCESSO}

Login sem sucesso
    Wait Until Element Is Visible       ${BOTAO_ENTRAR}
    Click Element                       ${BOTAO_ENTRAR}
    Wait Until Page Contains Element    ${MSG_SEM_DADOS}

Login sem senha
    [Arguments]                         ${email}

    Wait Until Element Is Visible       ${ELEMENT_EMAIL}
    Click Element                       ${ELEMENT_EMAIL}
    Execute Adb Shell                   input text                                                ${email}
    Hide Keyboard
    Click Element                       ${BOTAO_ENTRAR}
    Wait Until Page Contains Element    ${MSG_SEM_SENHA}

Esqueci a senha
    [Arguments]                         ${email}

    Wait Until Element Is Visible       ${ESQUECEU_SENHA}
    Click Element                       ${ESQUECEU_SENHA}
    Wait Until Element Is Visible       ${ELEMENT_EMAIL}
    Click Element                       ${ELEMENT_EMAIL}
    Execute Adb Shell                   input text                                                ${email}
    Hide Keyboard
    Click Element                       ${RECUPERAR_SENHA}
    Wait Until Element Is Visible       ${MSG_RECUPERA_SENHA}