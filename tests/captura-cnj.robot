***Settings***
Resource                ../resources/base.robot

Test Setup              Open Session
Test Teardown           Close Session

***Variables***
${BOTAO_TOGGLE}         xpath=//android.widget.Button[@content-desc="Toggle"]
${BOTAO_CAPTURA_CNJ}    xpath=//android.widget.ImageView[contains(@content-desc, 'Capturar pelo')]
${CAMPO_CNJ}            xpath=//android.widget.EditText[@text="Informe o N° CNJ do processo"]
${MSG_CNJ_BRANCO}       xpath=//android.view.View[@content-desc="O número do processo deve obedecer o padrão CNJ"]
${TEXT_CNJ}             Capturar processos pelo N° CNJ

***Test Cases***
Não deve permitir captura por CNJ sem informar número do processo
    [tags]                           captura-sem-cnj
    Go To Login Form
    Login com sucesso                root@projuris.com        pj2233      Casos
    Não informar CNJ

Não deve permitir captura por CNJ com número do processo incorreto
    [tags]                           captura-cnj-incorreto
    Go To Login Form
    Login com sucesso                root@projuris.com        pj2233      Casos
    Informar CNJ incorreto

***Keywords***
Não informar CNJ
    Wait Until Element Is Visible    ${BOTAO_TOGGLE}
    Click Element                    ${BOTAO_TOGGLE}
    Click Element                    ${BOTAO_CAPTURA_CNJ}
    Wait Until Page Contains         ${TEXT_CNJ}
    Click Element                    ${CAMPO_CNJ}
    Click Element At Coordinates     961                      370
    Wait Until Element Is Visible    ${MSG_CNJ_BRANCO}

Informar CNJ incorreto
    Wait Until Element Is Visible    ${BOTAO_TOGGLE}
    Click Element                    ${BOTAO_TOGGLE}
    Click Element                    ${BOTAO_CAPTURA_CNJ}
    Wait Until Page Contains         ${TEXT_CNJ}
    Click Element                    ${CAMPO_CNJ}
    Execute Adb Shell                input text               12345678
    Click Element At Coordinates     961                      370
    Wait Until Element Is Visible    ${MSG_CNJ_BRANCO}


