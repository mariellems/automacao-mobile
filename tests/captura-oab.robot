***Settings***
Resource                ../resources/base.robot

Test Setup              Open Session
Test Teardown           Close Session

***Variables***
${BOTAO_TOGGLE}         xpath=//android.widget.Button[@content-desc="Toggle"]
${BOTAO_CAPTURA_OAB}    xpath=//android.widget.ImageView[@content-desc="Capturar por OAB"]
${TEXT_OAB}             Capturar processos por OAB
${MSG_OAB_BRANCO}       O número da OAB precisa ser preenchido
${CAMPO_OAB}            class=android.widget.EditText
${MSG_UF_OAB_BRANCO}    A UF da OAB precisa ser preenchido

***Test Cases***
Não deve permitir captura por OAB sem informar dados
    [tags]                           captura-sem-oab
    Go To Login Form
    Login com sucesso                root@projuris.com        pj2233    Casos
    Não informar OAB

Não deve permitir captura por OAB com dados incorretos
    [tags]                           captura-oab-incorreto
    Go To Login Form
    Login com sucesso                root@projuris.com        pj2233    Casos
    Dados OAB incorretos

***Keywords***
Não informar OAB
    Wait Until Element Is Visible    ${BOTAO_TOGGLE}
    Click Element                    ${BOTAO_TOGGLE}
    Click Element                    ${BOTAO_CAPTURA_OAB}
    Wait Until Page Contains         ${TEXT_OAB}
    Click Element At Coordinates     951                      531
    Wait Until Page Contains         ${MSG_OAB_BRANCO}

Dados OAB incorretos
    Wait Until Element Is Visible    ${BOTAO_TOGGLE}
    Click Element                    ${BOTAO_TOGGLE}
    Click Element                    ${BOTAO_CAPTURA_OAB}
    Wait Until Page Contains         ${TEXT_OAB}
    Click Element                    ${CAMPO_OAB}
    Execute Adb Shell                input text               1234#
    Click Element At Coordinates     951                      531
    Wait Until Page Contains         ${MSG_UF_OAB_BRANCO}