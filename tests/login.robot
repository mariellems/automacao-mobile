***Settings***
Resource         ../resources/base.robot

Test Setup       Open Session
Test Teardown    Close Session

***Test Cases***
Deve logar com sucesso
    Go To Login Form
    Login com sucesso    root@projuris.com    pj2233    Casos

Login incorreto
    Go To Login Form
    Login incorreto      root@projuris.com    2233

Login sem informar email e senha
    Go To Login Form
    Login sem sucesso

Login sem informar senha
    Go To Login Form
    Login sem senha      root@projuris.com

Esqueci a senha
    Go To Login Form
    Esqueci a senha      root@projuris.com